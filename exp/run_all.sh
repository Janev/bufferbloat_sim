echo 'Simulating BB scenario'
sudo ./run.sh
echo 'Plotting BB observations'
./plot_figures.sh bb_observe 100

echo 'Running simulation with reduced queue size'
sudo ./run-minq.sh
echo 'Plotting graphs'
./plot_figures.sh bb_min_q 20

echo 'Running simulation with different queues'
sudo ./run-diff.sh
./plot_figures.sh bb_diff_q 100

echo 'Done Running experiments'

