.PHONY = all clean


all: run_experiment generate_report

#Run the experiment
run_experiment: configure
	@echo 'Running experiments, this may take a while...'
	cd exp;\
	sudo ./run_all.sh


#Produce pdf report
generate_report:
	@echo 'Generating pdf report with results'
	./generate_report.sh

#Satisfy all dependencies
configure:
	@echo 'calling configure'
	sudo ./configure.sh

clean: 
	@echo 'Removing experiment files, latex generated files and source caches'
	cd exp;\
	echo `pwd`;\
	rm *.aux *.pdf *.pyc;\
	rm *.png *_tcpprobe.txt *_sw0-qlen.txt;\


