#!/bin/bash

#Sets up:
#1. Mininet
#2. Matplotlib
#3. pdflatex

if [ `which mn` ]; then
	echo 'Mininet is installed'
else
	echo 'Mininet not found, downloading and installing now...'
	cd ~
	git clone git://github.com/mininet/mininet
	cd mininet
	./util/install.sh -fnv
	cd ~
fi
	
if `python -c "import matplotlib" &> /dev/null`; then
	echo 'Matplotlib detected'
else
	echo 'No Matplotlib found, installing matplotlib'
	sudo apt-get install -y python-matplotlib
fi

if [ `which pdflatex` ]; then
	echo 'pdflatex found'
else
	echo 'pdflatex not found, downloading and compliling now'
	sudo apt-get install -y texlive-latex-base
fi
